[![pipeline status](https://gitlab.com/general-julia/MyAux.jl/badges/master/pipeline.svg)](https://gitlab.com/general-julia/MyAux.jl/commits/master)
[![coverage report](https://gitlab.com/general-julia/MyAux.jl/badges/master/coverage.svg)](https://gitlab.com/general-julia/MyAux.jl/commits/master)

# MyAux

This is a package with helper functions of broad use.

