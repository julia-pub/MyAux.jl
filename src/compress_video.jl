# Compress video
# ffmpeg -i inputvid.mkv -vcodec libx265 -crf 30 outputvid.mp4

using ProgressMeter

get_mp4(folder; join=false) = filter(x -> last(splitext(x)) == ".mp4", readdir(folder; join))

function compress_video_folder(folder; minsize=10e6, kwargs...)
    fs = get_mp4(folder)
    cmpdir = joinpath(folder, "compressed")
    !isdir(cmpdir) && mkdir(cmpdir)
    setdiff!(fs, get_mp4(cmpdir))
    fullfs = map(fn -> joinpath(folder, fn), fs)
    sort!(fullfs; by=filesize);
    filter!(x -> filesize(x) > minsize, fullfs)
    @info "A total of $(length(fullfs)) files will be compressed" 

    @showprogress for fn in fullfs
        compress_video(fn; kwargs...)
        print(".")
    end
end

function compress_video(fn; compression_level=24)
    basedir, naked_fn = splitdir(fn)
    dir = joinpath(basedir, "compressed")
    !isdir(dir) && mkdir(dir)
    fnout = joinpath(dir, naked_fn)
    cmd = `ffmpeg -i $fn -vcodec libx265 -crf $compression_level $fnout`
    return run(cmd)
end
